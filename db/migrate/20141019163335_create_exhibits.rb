class CreateExhibits < ActiveRecord::Migration
  def change
    create_table :exhibits do |t|
      t.integer :user_id
      t.integer :type_id
      t.string :url
      t.boolean :private

      t.timestamps
    end
  end
end
