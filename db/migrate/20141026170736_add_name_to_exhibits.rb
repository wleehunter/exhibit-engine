class AddNameToExhibits < ActiveRecord::Migration
  def change
    add_column :exhibits, :name, :string
  end
end
