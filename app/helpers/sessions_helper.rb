module SessionsHelper

  def sign_in(user)
    cookies.permanent[:remember_token] = user.remember_token
    self.current_user = user
  end

  def sign_out
    self.current_user = nil
    cookies.delete(:remember_token)
  end

  def signed_in?
    !current_user.nil?
  end

  def current_user=(user)
    @current_user = user
  end

  def current_user
    @current_user ||= User.find_by_remember_token(cookies[:remember_token])
  end

  def require_signed_in
    if current_user.nil?
      redirect_to '/error/not-signed-in'
      return false
    elsif !current_user.auth_id.nil?  
      redirect_to '/error/email-auth'
      return false
    end 
    return true
  end

  def require_same_user
    redirect_to '/error/no-access' unless params[:id] == current_user.id.to_s
  end

  def require_email_auth
    redirect_to '/error/email-auth' unless current_user.auth_id.nil?
  end

  def require_exhibit_belongs_to_current_user
    exhibit = Exhibit.find_by_id(params[:exhibit_id])
    redirect_to '/error/no-access' unless (!current_user.nil? && !exhibit.nil? && exhibit.user_id == current_user.id)
    return false
  end
end
