json.array!(@exhibits) do |exhibit|
  json.extract! exhibit, :id, :user_id, :type_id, :url, :private
  json.url exhibit_url(exhibit, format: :json)
end
