json.array!(@assets) do |asset|
  json.extract! asset, :id, :url, :order
  json.url asset_url(asset, format: :json)
end
