json.array!(@users) do |user|
  json.extract! user, :id, :username, :email, :password_digest, :auth_id, :admin, :remember_token
  json.url user_url(user, format: :json)
end
