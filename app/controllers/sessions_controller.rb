class SessionsController < ApplicationController
  def new

  end

  def create
    if !params[:session][:email].nil?
      email_param = params[:session][:email].downcase
    else
      email_param = ''
    end
    user = User.find_by_email(email_param)
    if user && user.authenticate(params[:session][:password])
      puts user.inspect
      sign_in user
      redirect_to user
    else
      flash[:error] = 'Invalid email/password combination'
      redirect_to root_url
      # render 'new'
    end
  end

  def destroy 
    sign_out
    redirect_to root_url
  end
end
