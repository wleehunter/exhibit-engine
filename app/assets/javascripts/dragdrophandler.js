var DragDropHandler = {
  $status: null,
  $manualTrigger: null,
  $manualInput: null,
  $dropContainer: null,
  $list: null,
  $selected: null,
  $assetForm: null,
  $name: null,
  $data: null,
  $uploadDialogue: null,
  currentFile: null,
  fileList: {},

  acceptedTypes: {
    'image/png': true,
    'image/jpeg': true,
    'image/gif': true
  },

  init: function() {
    this.$status = $('#status');
    this.$manualTrigger = $('#manual-file-trigger');
    this.$manualInput = $('#manual-file');
    this.$dropContainer = $('#drop-container');
    this.$list = $('#images');
    this.$selected = this.$list.find('.selected');
    this.$assetForm = $('#new_asset');
    this.$name = this.$assetForm.find('#name');
    this.$data = this.$assetForm.find('#data');
    this.$uploadDialogue = $('#upload-dialogue');
    if(window.FileReader) {
      this.addEventListeners();

    }
  },

  addEventListeners: function() {
    var _this = this;
    this.$manualTrigger.click(function(event){
      _this.cancel(event);
      _this.$manualInput.trigger('click');
    })
    this.$dropContainer.on('dragover', function(event){
      _this.cancel(event);
    });
    this.$dropContainer.on('dragenter', function(event){
      _this.cancel(event);
      $(this).addClass('hover');
    });
    this.$dropContainer.on('drop', function(event){
      _this.cancel(event);
      $(this).removeClass('hover');
      _this.onFileDrop(event);
    });
    $(document).on('click','#images li',function(event){
      _this.cancel(event);
      _this.onListItemClick(event);
    });
    $(document).on('click','a#upload-trigger',function(event){
      _this.cancel(event);
      _this.onUploadTriggerClick(event);
    });
  },

  onFileDrop: function(event) {
    var dropHandler = this;
    if(event.originalEvent.dataTransfer && event.originalEvent.dataTransfer.files.length){
      var files = event.originalEvent.dataTransfer.files;
      for(var i = 0; i < files.length; i++) {
        var currentFile = files[i];
        if(this.acceptedTypes[currentFile.type]) {
          var reader = new FileReader();
          reader.onloadend = (function(file){
            return function(event) {
              dropHandler.addImageToList(event, file)
            };
          })(currentFile);



          // $(reader).on('loadend', function(event){
          //   var imageData = event.target.result;
          //   dropHandler.addImageToList(currentFile, imageData)
          // })
          reader.readAsDataURL(currentFile);
        }
      }
    }
  },

  addImageToList: function(event, file) {
    console.log(file)
    var data = event.target.result;
    this.fileList[file.name] = {
      name: file.name,
      file: file,
      selected: true
    };
    var $item = $('<li>').addClass('selected');
    var $img = $('<img>');
    var $mask = $('<div>').addClass('mask');
    $img.attr('src',data);
    $img.attr('fileName', file.name);
    $item.append($img);
    $item.append($mask);
    this.$list.append($item);
    var orientation = $img.width() >= $img.height() ? 'horizontal' : 'vertical';
    $img.addClass(orientation);
    if(this.$list.find('.selected').length > 0) {
      this.$uploadDialogue.show();
    }
  },

  onListItemClick: function(event) {
    var _this = this;
    var $item = $(event.currentTarget);
    var fileName = $item.find('img').attr('fileName');
    $item.toggleClass('selected');
    if($item.hasClass('selected')) {
      this.fileList[fileName].selected = true;
    }
    else {
      this.fileList[fileName].selected = false;
    }
  },

  onUploadTriggerClick: function(event) {
    var $selected = this.$list.find('.selected');
    var files = [];
    $selected.each(function(){
      files.push($(this).find('img').attr('file'));
    });
    console.log(files)
  },

  uploadFile: function(file, fileData) {
    var formData = new FormData();
    // formData.append('name', file.name);
    // formData.append('data', fileData);
    formData.append('file', file);
    $.ajax({
        type: "POST",
        // beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))},
        url: "/save-asset.json",
        dataType: 'JSON',
        processData: false,
        contentType: false,
        data: formData,
        success: function(data) {
          console.log(data);
        }
      });
  },

  cancel: function(event) {
    event.stopPropagation();
    event.preventDefault();
    return false;
  }

}
    
$(document).ready(function(){
  DragDropHandler.init();
});





