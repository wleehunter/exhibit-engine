# service that pushes files to amazon s3
class Push
  require 'rubygems/package'
  require 'json'
  require 'fileutils'
  require 'filetools'
  require 'zlib'
  require 'open-uri'

  @@TMP = 'public/tmp'

  def to_s3 name, file
    bucket = s3.buckets[S3_BUCKET_NAME]
 
    type = get_content_type file
    if should_compress(file)
      Dir.chdir(@@TMP)
      Zlib::GzipWriter.open(name + '.gz') do |gangsta_zip|
        gangsta_zip.write(file)
      end
      object = push_object(name, open(@@TMP "/" name + '.gz'), type, true);
      # TODO delete it
    else
      object = bucket.objects.create(s3_name, file, type, false );
    end

    @pushed << object





    return @pushed
  end

  def push_object file_name, data, type, should_compress
    s3 = AWS::S3.new
    bucket = s3.buckets[S3_BUCKET_NAME]
    if should_compress
      resp = bucket.objects.create(file_name, data, :content_type => type, :content_encoding => 'gzip')
    else
      resp = bucket.objects.create(file_name, data, :content_type => type)
    end
    return resp
  end

  def get_content_type file
    # extension = get_ext file
    # case extension
    # when "js"
    #   content_type = "application/javascript"
    # when "json"
    #   content_type = "application/json"
    # when "html"
    #   content_type = "text/html"
    # when "xml"
    #   content_type = "text/xml"
    # when "css"
    #   content_type = "text/css"
    # when "txt"
    #   content_type = "text/plain"
    # when "jpg"
    #   content_type = "image/jpeg"
    # when "png"
    #   content_type = "image/png"
    # when "gif"
    #   content_type = "image/gif"
    # when "svg"
    #   content_type = "application/octet-stream"
    # else
    #   content_type = nil
    # end
    return FileMagic.new(FileMagic::MAGIC_MIME).file(file)
  end

  def should_compress file
    type = get_content_type file
    compress = false
    case type
    when "application/javascript"
      compress = true
    when "application/json"
      compress = true
    when "text/html"
      compress = false
    when "text/xml"
      compress = true
    when "text/css"
      compress = true
    when "text/plain"
      compress = true
    else
      compress = false
    end
    return compress
  end

end