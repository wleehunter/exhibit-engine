# service that pushes files to amazon s3
class Push
  require 'rubygems/package'
  require 'json'
  require 'fileutils'
  # require 'filetools'
  require 'zlib'
  require 'open-uri'

  @@TMP = Rails.root.to_s + '/public/tmp'

  def to_s3 name, data
    file_name = @@TMP + "/" + name
    Dir.chdir(@@TMP)
    file = File.new(name, "w+")
    file.write(data)
    file.close
    type = get_content_type(file_name)

    if should_compress(file_name)
      Zlib::GzipWriter.open(name + '.gz') do |gangsta_zip|
        gangsta_zip.write(file)
      end
      object = push_object(name, open(file_name + '.gz'), type, true);
      FileUtils.rm(file_name + '.gz', :force => true)
    else
      object = push_object(name, open(file_name), type, false );
    end
    # FileUtils.rm(file_name, :force => true)

    return object
  end

  def push_object file_name, data, type, should_compress
    s3 = AWS::S3.new
    bucket = s3.buckets[S3_BUCKET_NAME]
    resp = nil
    if should_compress
      resp = bucket.objects.create(file_name, data, :content_type => type, :content_encoding => 'gzip')
    else
      puts "------------------------------------"
      puts data
      puts file_name
      puts type
      resp = bucket.objects.create(file_name, data, :content_type => type)
    end
    return resp
  end

  def get_content_type file
    # extension = get_ext file
    # case extension
    # when "js"
    #   content_type = "application/javascript"
    # when "json"
    #   content_type = "application/json"
    # when "html"
    #   content_type = "text/html"
    # when "xml"
    #   content_type = "text/xml"
    # when "css"
    #   content_type = "text/css"
    # when "txt"
    #   content_type = "text/plain"
    # when "jpg"
    #   content_type = "image/jpeg"
    # when "png"
    #   content_type = "image/png"
    # when "gif"
    #   content_type = "image/gif"
    # when "svg"
    #   content_type = "application/octet-stream"
    # else
    #   content_type = nil
    # end
    return FileMagic.new(FileMagic::MAGIC_MIME).file(file)
  end

  def should_compress file
    type = get_content_type file
    compress = false
    case type
    when "application/javascript"
      compress = true
    when "application/json"
      compress = true
    when "text/html"
      compress = false
    when "text/xml"
      compress = true
    when "text/css"
      compress = true
    when "text/plain"
      compress = true
    else
      compress = false
    end
    return compress
  end

end